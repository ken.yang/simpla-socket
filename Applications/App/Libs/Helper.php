<?php

/*
 * 帮助类
 */

namespace \App\Libs;


class Helper {

    /**
     * 获取今天的开始和结束时间
     * @return array
     */
    public static function getTodayStartEndTime() {
        $t = time();
        //开始时间戳
        $beginTime = mktime(0, 0, 0, date("m", $t), date("d", $t), date("Y", $t));
        //结束时间戳
        $endTime = mktime(23, 59, 59, date("m", $t), date("d", $t), date("Y", $t));

        return [$beginTime, $endTime];
    }

    /**
     * 获取本周的开始和结束时间
     * @return array
     */
    public static function getWeekStartEndTime() {
        $week_audit_num = 0;
        $week_use_num = 0;
        $time = '1' == date('w') ? strtotime('Monday', time()) : strtotime('last Monday', time());
        $beginTime = strtotime(date('Y-m-d 00:00:00', $time));
        $endTime = strtotime(date('Y-m-d 23:59:59', strtotime('Sunday', time())));
        return [$beginTime, $endTime];
    }

    /**
     * 获取本月的开始和结束时间
     * @return array
     */
    public static function getMonthStartEndTime() {
        $month_audit_num = 0;
        $month_use_num = 0;
        $beginTime = date('Y-m-d 00:00:00', mktime(0, 0, 0, date('m', time()), '1', date('Y', time())));
        $endTime = date('Y-m-d 23:39:59', mktime(0, 0, 0, date('m', time()), date('t', time()), date('Y', time())));
        $beginTime = strtotime($beginTime);
        $endTime = strtotime($endTime);
        return [$beginTime, $endTime];
    }

    /**
     * 根据天数num获取星期几
     * @param $weekNum
     * @return string
     */
    public static function getWeekName($weekNum) {
        switch ($weekNum) {
            case 0:
                $weekName = '星期天';
                break;
            case 1:
                $weekName = '星期一';
                break;
            case 2:
                $weekName = '星期二';
                break;
            case 3:
                $weekName = '星期三';
                break;
            case 4:
                $weekName = '星期四';
                break;
            case 5:
                $weekName = '星期五';
                break;
            case 6:
                $weekName = '星期六';
                break;
        }

        return $weekName;
    }

    /**
     * 根据时间获取上午还是下午
     * @param $dayTime
     * @return string
     */
    public static function getDayOf($dayTime) {
        $dayName = '上午';
        if ($dayTime > "12:00") {
            $dayName = '下午';
        }

        return $dayName;
    }

    public static function getOneDayAreaTime() {
        $areaTime = [];
        $startTime = strtotime(date("Y-m-d", time()));
        for ($i = 0; $i < 24; $i++) {
            $currentTime = $startTime + $i * 60 * 60;
            $areaTime[Date("H", $currentTime)] = 0;
        }
        return $areaTime;
    }

}
