<?php

namespace \App\Controller;

use \App\Controller\Controller;
use \App\Models\Test;

class TestController Extends Controller{
    public function sayHello() {
        $data = Test::sayHello();
        return $this->output->setData($data)->json();
    }
}